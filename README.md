## HTTP/S Reverse Proxy
 A project I worked on for the "SE201 - Introduction to Software Engineering" class.

## About The Project


The application is written in OOP PHP using the Model View Controller pattern, which interacts with the nginx web server and MySQL database to allow pass-through of web traffic.

## Technology used

* PHP 8.0
* MariaDB
* NGINX web server


## License

Distributed under the GNU GPLv3 License. See [LICENSE](https://bitbucket.org/blackholered/http-s-reverse-proxy/src/main/LICENSE.md) for more information.

