<?php
include "../se201/utilities/Validation.php";
class Testing extends \PHPUnit\Framework\TestCase {

    public function testUsername() {
        $result = Validation::validateUsername("Test123");

        $this->assertEquals(true, $result);
    }
    public function testUsernameBad() {
        $result = Validation::validateUsername("Test");

        $this->assertEquals(false, $result);
    }

    public function testPassword() {
        $result = Validation::validatePassword("Test12345");

        $this->assertEquals(true, $result);
    }
    public function testPasswordBad() {
        $result = Validation::validatePassword("Test123");

        $this->assertEquals(false, $result);
    }

    public function testDomain() {
        $result = Validation::verifyDomain("example.com");

        $this->assertEquals(true, $result);
    }

    public function testDomainBad() {
        $result = Validation::verifyDomain("bad_domain");

        $this->assertEquals(false, $result);
    }

    public function testIP() {
        $result = Validation::verifyIP("127.0.0.1");

        $this->assertEquals(true, $result);
    }
    public function testIPBad() {
        $result = Validation::verifyIP("example");

        $this->assertEquals(false, $result);
    }

}



?>