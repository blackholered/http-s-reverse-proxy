<?php
  class DatabaseConnection {
    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance)) {
          $opt = array(
              PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
              PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
          );
        self::$instance = new PDO('mysql:host=localhost;dbname=se201-projekat', '', '', $opt);
      }
      if (self::$instance::ATTR_CONNECTION_STATUS == 0) {
          die("Failed to connect to database");
      }
      return self::$instance;
    }
  }
?>