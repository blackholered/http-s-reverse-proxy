<?php
require_once("models/Authentication.php");
require_once("models/Domain.php");


class AdminController
{

    private Domain $domain;
    public function __construct(string $action)
    {

        $this->domain = new Domain();

        if (!Authentication::isAdmin()) {
            header('Location: ./?page=domains&action=viewall');
            die("User is not an administrator");
        }

        switch ($action) {
            case "view":
                $this->view();
                break;
        }

    }

    private function view(): void {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['domain_name'])) {
            try {
                $this->domain->setName($_POST['domain_name']);
                $this->domain->suspendDomain();
                $this->domain->removeDomainRule();
                $this->ex = "Domain suspended successfully";
            } catch (Exception $e) {
                $this->ex = $e->getMessage();
            }
        }
        require_once('views/pages/AdminView.php');
    }


}