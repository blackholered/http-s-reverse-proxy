<?php
require_once("models/Authentication.php");

class AuthenticationController
{

    private Authentication $auth;
    private string $ex;

    public function __construct(String $action)
    {

        if (Authentication::isAuthenticated() && !Authentication::isAdmin() && strcmp($action, "signout")) {
            echo $action;
            header('Location: ./?page=domain&action=viewall');
            die("User is already logged in");
        }
       if (Authentication::isAdmin() && strcmp($action, "signout")) {
            header('Location: ./?page=admin&action=view');
        }

        switch ($action) {
            case "login":
                $this->login();
                break;
            case "register":
                $this->register();
                break;
            case "signout";
            $this->signout();
            break;
            case "error";
            $this->error();
            break;
        }

    }


    private function login(): void
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['login'])) {
            try {
                $this->auth = Authentication::construct($_POST['username'], $_POST['password'], "login");

                if ($this->auth->login()) {
                    $_SESSION["id"] = $this->auth->getID();
                    $_SESSION["username"] = $this->auth->getUsername();
                    $_SESSION["admin"] = $this->auth->getAdmin();
                    if ($this->auth->getAdmin()) {
                        header('Location: ./?page=admin&action=view');
                    } else {
                        header('Location: ./?page=domain&action=viewall');

                    }
                    }
            } catch (Exception $e) {
                $this->ex = $e->getMessage();
            }

        }
            require_once('views/pages/LoginView.php');
        }



    private function register(): void
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['register'])) {
            try {
                $this->auth = Authentication::construct($_POST['username'], $_POST['password'], "create");

                if ($this->auth->createUser()) {
                    header('Location: ./?page=authentication&action=login');
                }

            } catch (Exception $e) {
                $this->ex = $e->getMessage();
            }

        }
        require_once('views/pages/CreateView.php');

    }

    private function signout() {
        Authentication::destroySession();
        header('Location: ./?page=authentication&action=login');
    }
    private function error() {
        $this->ex = "404 - This page does not exist";
        require_once('views/pages/ErrorView.php');
    }


}