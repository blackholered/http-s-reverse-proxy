<?php
require_once("models/Authentication.php");
require_once("models/Domain.php");
class DomainController
{

    private Domain $domain;
    private User $user;
    private Array $allDomains;
    private String $ex;


    public function __construct(string $action)
    {
        if (!Authentication::isAuthenticated() || Authentication::isAdmin()) {
            header('Location: ./?page=authentication&action=login');
            die("User is not logged in");
        }
        $this->domain = new Domain();
        $this->user = new User();
        $this->user->setID($_SESSION['id']);



        switch ($action) {
            case "viewall":
                $this->viewAll();
                break;
            case "manage":
                $this->manage();
                break;
            case "add":
                $this->add();
                break;
            case "delete":
                $this->delete();
                break;

        }
    }

    public function getDomain(): Domain {
        return $this->domain;
    }


    private function delete(): void {
        if (isset($_GET['id'])) {
            try {
                $this->domain->setId($_GET['id']);
                $this->domain =  $this->domain->getDomainByUserId($this->user->getID());

                $this->domain->removeDomain();
                    $this->domain->removeDomainRule();
                    header('Location: ./?page=domain&action=viewall');
            } catch (Exception $e) {
                $this->ex = $e->getMessage();
                require_once('views/pages/ErrorView.php');
            }

        }
        }


    private function manage(): void
    {
        if (isset($_GET['id'])) {
            try {
                $this->domain->setId($_GET['id']);
                $this->domain = $this->domain->getDomainByUserId($this->user->getID());
                if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['ip_address']) && isset($_POST['public_key']) && isset($_POST['private_key'])) {
                    $this->domain = $this->domain->manageConstruct($this->domain->getId(), $this->domain->getName(), $_POST['ip_address'], $_POST['public_key'], $_POST['private_key'], $this->domain->isActive());
                    $this->domain->updateDatabase();
                        $this->domain->formatSSLConfig();
                        $this->domain->createRule();
                        header('Location: ./?page=domain&action=viewall');
                }
            } catch (Exception $e) {
                $this->ex = $e->getMessage();
            }
        }
        require_once('views/pages/ManageDomainView.php');
    }


    private function add(): void
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['domain_name']) && isset($_POST['ip_address']) && isset($_POST['public_key']) && isset($_POST['private_key'])) {
            try {
                $this->domain = $this->domain->createConstruct($_POST['domain_name'], $_POST['ip_address'], $_POST['public_key'], $_POST['private_key']);
                $this->domain->insertDatabase($this->user->getID());
                    $this->domain->formatSSLConfig();
                    $this->domain->createRule();
                    header('Location: ./?page=domain&action=viewall');
            } catch (Exception $e) {
                $this->ex = $e->getMessage();

            }
        }
        require_once('views/pages/AddDomainView.php');
    }



        private function viewALl(): void
        {
          $this->allDomains = $this->domain->getAllDomains($this->user->getID());
          require_once('views/pages/AllDomainView.php');
        }


}