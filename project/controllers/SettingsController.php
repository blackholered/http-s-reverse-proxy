<?php
require_once("models/Authentication.php");
require_once("models/User.php");
require_once("models/Domain.php");
class SettingsController
{
private User $user;
private Domain $domain;
private String $ex;


    public function __construct(string $action)
    {
        if (!Authentication::isAuthenticated()) {
            header('Location: ./?page=authentication&action=login');
            die("User is not logged in");
        }


        $this->user = new User();
        $this->user->setID($_SESSION['id']);

        $this->domain = new Domain();


        switch ($action) {
            case "view":
                $this->view();
                break;
            case "delete":
                $this->delete();
                break;

        }

    }
    private function view(): void {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['new_password'])) {
            try {
                $this->user->setPasswordCreate($_POST['new_password']);
                $this->user->updatePassword();
                header('Location: ./?page=domain&action=viewall');
            } catch (Exception $e) {
                $this->ex = $e->getMessage();
            }

        }
        require_once('views/pages/SettingsView.php');
    }

    private function delete(): void {
        try {
            $allDomains = $this->domain->getAllDomains($this->user->ge.tID());
            foreach ($allDomains as $domain) {
                $domain->removeDomain();
                $domain->removeDomainRule();
            }
            $this->user->removeUser();
            Authentication::destroySession();
            header('Location: ./?page=authentication&action=login');
        } catch (Exception $e) {
            $this->ex = $e->getMessage();
            require_once('views/pages/SettingsView.php');
        }



    }



}