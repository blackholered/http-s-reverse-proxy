<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>



<div class="container px-4 py-5" id="featured-3">

    <table class="table">
        <thead class="table-dark">
        <tr>
            <th class="text-center" scope="col">Status</th>
            <th class="text-center" scope="col">Domain Name</th>
            <th class="text-center" scope="col">IPv4 Address</th>
            <th class="text-center" scope="col">SSL Certificate</th>
            <th class="text-center" scope="col">Options</th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($this->allDomains as $domain) {
            echo "<tr>";
            if ($domain->isActive()) {
                echo "<td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-check2-circle' viewBox='0 0 16 16'>
                    <path d='M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z'/>
                    <path d='M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z'/>
                </svg></td>
            <td class='text-center'><h5>" . $domain->getName(). "</h5></td>
            <td class='text-center'><h5>" . $domain->getIp() . "</h5></td>
           ";
                if (!empty($domain->getCertificate())) {
echo "                <td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-check' viewBox='0 0 16 16'>
                    <path d='M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z'/>
                </svg></td>";
                } else {
                    echo "<td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-x' viewBox='0 0 16 16'>
  <path d='M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z'/>
</svg></td>
";
                }

        echo "<td class='text-center'><a href='?page=domain&action=manage&id=" . $domain->getId(). "' type='button' class='btn btn-warning'>Manage</a><a href='?page=domain&action=delete&id=" . $domain->getId(). "'' type='button' class='btn mx-2 btn-danger'>Delete</a></td>";
            } else {
                echo "<td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-exclamation-octagon' viewBox='0 0 16 16'>
  <path d='M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1 1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z'/>
  <path d='M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z'/>
</svg></td>
            <td class='text-center'><h5>" . $domain->getName(). "</h5></td>
            <td class='text-center'><h5>" . $domain->getIp() . "</h5></td>
           
";
                if (!empty($domain->getCertificate())) {
                    echo "                <td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-check' viewBox='0 0 16 16'>
                    <path d='M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z'/>
                </svg></td>";
                } else {
                    echo "<td class='text-center'><svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-x' viewBox='0 0 16 16'>
  <path d='M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z'/>
</svg></td>
";

                }
                echo "<td class='text-center'><a href='?page=domain&action=manage&id=" . $domain->getId(). "' type='button' class='btn btn-warning'>Manage</a><a href='?page=domain&action=delete&id=" . $domain->getId(). "'' type='button' class='btn mx-2 btn-danger'>Delete</a></td>";


            }
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

</div>