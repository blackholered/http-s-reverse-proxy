<!-- Custom styles for this template -->
<link href="./assets/dist/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">


<main class="form-signin" >

    <form method="POST">
        <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" fill="currentColor" class="bi bi-cloud-fog2-fill" viewBox="0 0 16 16">
            <path d="M8.5 3a5.001 5.001 0 0 1 4.905 4.027A3 3 0 0 1 13 13h-1.5a.5.5 0 0 0 0-1H1.05a3.51 3.51 0 0 1-.713-1H9.5a.5.5 0 0 0 0-1H.035a3.53 3.53 0 0 1 0-1H7.5a.5.5 0 0 0 0-1H.337a3.5 3.5 0 0 1 3.57-1.977A5.001 5.001 0 0 1 8.5 3z"/>
        </svg>        <h1 class="h3 mb-3 fw-normal">Create an account</h1>
        <?php
        if (!empty($this->ex)) {
            echo "<div class='alert alert-danger' role='alert'>
  " . $this->ex . "
</div>";
        }?>
        <div class="form-floating">
            <input type="username" name="username" class="form-control" id="username" placeholder="test123">
            <label for="username">Username</label>
        </div>
        <div class="form-floating">
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            <label for="password">Password</label>
        </div>
        <input type="hidden" value="register" name="register" id="register" />

        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2022</p>
    </form>
</main>
</body>