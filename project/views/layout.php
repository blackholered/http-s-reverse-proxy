    <?php require_once('Routes.php');

    $route = new Routes($controller, $action);
    ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Headers · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/headers/">



    <!-- Bootstrap core CSS -->
    <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="./assets/headers.css" rel="stylesheet">
</head>
<body>
<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" fill="currentColor" class="bi bi-cloudy-fill" viewBox="0 0 16 16">
                    <path d="M13.405 7.027a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 13H13a3 3 0 0 0 .405-5.973z"/>
                </svg>
            </a>

            <?php
            if (!isset($_SESSION["id"])) {
                echo "            <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>
            </ul>
        <div class='text-end'>
          <a href='./?page=authentication&action=login' type='button' class='btn btn-outline-light me-2'>Login</a>
          <a href='./?page=authentication&action=register' type='button' class='btn btn-warning'>Sign-up</a>
        </div>
            
            ";
            } elseif (isset($_SESSION["id"]) && !$_SESSION['admin']) {
                echo "            <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>
                <li><a href='./?page=domain&action=viewall' class='nav-link px-2 text-white'>View Domains</a></li>
                <li><a href='./?page=domain&action=add' class='nav-link px-2 text-white'>Add Domain</a></li>
                <li><a href='./?page=settings&action=view' class='nav-link px-2 text-white'>Settings</a></li>
                
            </ul>
            
            <div class='text-end'>
            <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>
     <li><p class='mb-1 nav-link px-2 text-white'>Welcome, " . $_SESSION["username"] . "</p></li>
                 <a href='./?page=authentication&action=signout' class='d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none'>
 <svg href='test.php' xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='mb-1 bi bi-box-arrow-left' viewBox='0 0 16 16'>
    <path fill-rule='evenodd' d='M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z'/>
    <path fill-rule='evenodd' d='M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z'/>
  </svg></a>

     </ul>
</div>
            
            ";
            } elseif (isset($_SESSION["id"]) && isset($_SESSION['admin'])) {
                echo "            <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>
                <li><a href='./?page=admin&action=view' class='nav-link px-2 text-white'>Suspend Domains</a></li>
                <li><a href='./?page=settings&action=view' class='nav-link px-2 text-white'>Settings</a></li>
                
            </ul>
            
            <div class='text-end'>
            <ul class='nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0'>
     <li><p class='mb-1 nav-link px-2 text-white'>Welcome, " . $_SESSION["username"] . "</p></li>
                 <a href='./?page=authentication&action=signout' class='d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none'>
 <svg href='test.php' xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='mb-1 bi bi-box-arrow-left' viewBox='0 0 16 16'>
    <path fill-rule='evenodd' d='M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z'/>
    <path fill-rule='evenodd' d='M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z'/>
  </svg></a>

     </ul>
</div>";
            }
            ?>

        </div>
    </div>
</header>

<?php
    $route->add();

    ?>


<script src="./assets/dist/js/bootstrap.bundle.min.js"></script>


</body>
</html>