<?php

class Validation
{

    static function validateUsername(String $username) {
        if(preg_match('/^\w{5,}$/', $username)) {
            return true;
        }
    }

    static function validatePassword(String $password) {
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            return false;
        }
        return true;
    }

    static function hashPassword(String $password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }


    static function verifyDomain(String $domain_name)
    {
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
            && preg_match("/^.{1,253}$/", $domain_name) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
    }

    static function verifyIP(String $ip) {
        if(filter_var($ip, FILTER_VALIDATE_IP))
        {
            return true;
        }
    }

    static function checkCertificate(String $public, String $private) {


        return openssl_x509_check_private_key($public, $private);


    }

}