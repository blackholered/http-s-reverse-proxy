<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  if (isset($_GET['page']) && isset($_GET['action'])) {
    $controller = $_GET['page'];
    $action     = $_GET['action'];
  } else {
    $controller = 'authentication';
    $action     = 'login';
  }

  require_once('views/layout.php');
?>