<?php
require_once('DatabaseConnection.php');
class Routes
{
    private String $controller;
    private String $action;

    public function __construct($controller, $action) {
        $this->controller = $controller;
        $this->action = $action;
        DatabaseConnection::getInstance();
        session_start();
    }

     private function call(): void
    {
        require_once('controllers/' . ucfirst($this->controller) . 'Controller.php');

        switch ($this->controller) {
            case 'authentication':
                $controller = new AuthenticationController($this->action);
                break;
            case 'domain':
                $controller = new DomainController($this->action);
                break;
            case 'settings':
                $controller = new SettingsController($this->action);
                break;
            case 'admin': {
                $controller = new AdminController($this->action);
            }

        }
        //$controller->{$this->action}();
    }

     function add() {
        // we're adding an entry for the new controller and its actions
        $controllers = array('home' => ['view', 'error'],
            'authentication' => ['login', 'register', 'signout', 'error'],
            'domain' => ['viewall', 'manage', 'add', 'delete'],
            'settings'=> ['view', 'delete'],
            'admin' => ['view']);

        if (array_key_exists($this->controller, $controllers)) {
            if (in_array($this->action, $controllers[$this->controller])) {
                $this->call();
            } else {
                $this->controller = "authentication";
                $this->action = "error";
                $this->call();
            }
        } else {
            $this->controller = "authentication";
            $this->action = "error";
            $this->call();
        }
    }
}
?>