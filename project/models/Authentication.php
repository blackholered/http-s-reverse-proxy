<?php
require_once("User.php");

class Authentication extends User
{


    /**
     * @throws Exception
     */
    public static function construct(string $username, string $password, string $action): Authentication
    {
        $authentication = new Authentication();
        if (!strcmp($action, "create")) {
            $authentication->setUsernameCreate($username);
            $authentication->setPasswordCreate($password);
        } else if (!strcmp($action, "login")) {
            $authentication->setUsernameLogin($username);
            $authentication->setPasswordLogin($password);
        } else {
            throw new Exception("Cannot proceed because an undefined action value was supplied. Contact the developer");
        }
        $authentication->setDatabaseInstance(DatabaseConnection::getInstance());
        return $authentication;
    }

    /**
     * @throws Exception
     */
    public function login(): bool
    {
        $req = $this->getDatabaseInstance()->prepare('SELECT id, password, admin FROM users WHERE username = :username');
        $req->execute(array('username' => $this->getUsername()));

        $result = $req->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            if (password_verify($this->getPassword(), $result['password'])) {
                $this->setID($result['id']);
                $this->setAdmin($result['admin']);
                return true;
            } else {
                throw new Exception("Password is incorrect");
            }
        }
        throw new Exception("User does not exist");
    }


    /**
     * @throws Exception
     */
    public function createUser(): bool
    {

        // check whether this username already exists
        $req = $this->getDatabaseInstance()->prepare('SELECT username FROM users WHERE username = :username');
        $req->execute(array('username' => $this->getUsername()));
        $result = $req->fetch(PDO::FETCH_ASSOC);
        if (!$result) { // no user with this username exists, proceed
            $req = $this->getDatabaseInstance()->prepare('INSERT INTO users (username, password) VALUES (:username, :password)');
            $req->execute(array('username' => $this->getUsername(), 'password' => $this->getPassword()));
            return true;
        }
        throw new Exception("This user already exists!");
    }

    public static function isAuthenticated(): bool {
        if (isset($_SESSION["id"])) {
            $req = DatabaseConnection::getInstance()->prepare('SELECT username FROM users WHERE id = :id');
            $req->execute(array('id' => $_SESSION["id"]));
            $result = $req->fetch(PDO::FETCH_ASSOC);
            if (strcmp($result['username'], $_SESSION["username"])) {
               self::destroySession();
                return false;
            }
            return true;
        }
        return false;
    }

    public static function isAdmin(): bool {
        if (isset($_SESSION["admin"]) && $_SESSION["admin"]) {
            return true;
        }
        return false;
    }

    public static function destroySession(): void {
        $_SESSION = array();
        session_destroy();
    }




}