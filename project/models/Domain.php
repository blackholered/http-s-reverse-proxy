<?php

class Domain
{

    private PDO $databaseInstance;
    private int $id;
    private string $name;
    private string $ip;
    private string $certificate;
    private string $private_key;
    private bool $active;
    private String $config;


    public function __construct()
    {
        $this->databaseInstance = DatabaseConnection::getInstance();

    }

    /**
     * @param String $name
     * @param String $ip
     * @param String $certificate
     * @param String $private_key
     * @return Domain
     * @throws Exception
     */
    public function createConstruct(String $name, String $ip, String $certificate, String $private_key): Domain  {
        $domain = new Domain();
        $domain->setName($name);
        $domain->setIp($ip);
        $domain->validateCertificates($certificate, $private_key);
        return $domain;
    }

    public function manageConstruct(int $id, String $name, String $ip, String $certificate, String $private_key, bool $active): Domain  {
        $domain = new Domain();
        $domain->setID($id);
        $domain->setName($name);
        $domain->setIp($ip);
        $domain->validateCertificates($certificate, $private_key);
        $domain->setActive($active);
        return $domain;
    }

    public function validateCertificates(string $public, string $private): void
    {
        if (Validation::checkCertificate($public, $private)) {
            $this->setCertificate($public);
            $this->setPrivateKey($private);
        } elseif (empty($public) && empty($private)) {
            $this->setCertificate($public);
            $this->setPrivateKey($private);
        } else {
            throw new Exception("SSL Certificates are not valid");
        }
    }

    public function getDomainByUserId(int $userID): Domain {
        $domain = new Domain();
        $req = $this->databaseInstance->prepare('SELECT * FROM domains WHERE id = :id');
        $req->execute(array('id' => $this->getId()));
        $result = $req->fetch(PDO::FETCH_ASSOC);
        if ($result && $result['user'] == $userID) {
            $domain->setID($this->getId());
            $domain->setName($result['domain_name']);
            $domain->setIp($result['ip_address']);
            $domain->setCertificate($result['ssl_certificate']);
            $domain->setPrivateKey($result['private_key']);
            $domain->setActive($result['active']);
            return $domain;
        }
            throw new Exception("You're not authorized to manage this domain");
        }


    public function insertDatabase(int $userID): bool {
        // check whether this username already exists
        $req = $this->databaseInstance->prepare('SELECT domain_name FROM domains WHERE domain_name = :domain');
        $req->execute(array('domain' => $this->getName()));
        $result = $req->fetch(PDO::FETCH_ASSOC);
        if (!$result) { // no user with this username exists, proceed
            $req = $this->databaseInstance->prepare('INSERT INTO domains (user, domain_name, ip_address, ssl_certificate, private_key, active) VALUES (:user, :domain_name, :ip_address, :ssl_certificate, :private_key, :active)');
           if ($req->execute(array('user' => $userID, 'domain_name' => $this->getName(), "ip_address" => $this->getIp(), "ssl_certificate" => $this->getCertificate(), "private_key" => $this->getPrivateKey(), "active"=> 1))) {
               return true;
           }
        } else {
            throw new Exception("This domain name is already registered. Please contact support.");
        }
        throw new Exception("Failed to create domain. Please contact support");
    }

    public function updateDatabase(): bool {
        if (!$this->isActive()) {
            throw new Exception("You cannot manage this domain because it is suspended. Please contact support.");
        }
        $req = $this->databaseInstance->prepare('UPDATE domains SET ip_address = :ip_address, ssl_certificate = :ssl_certificate, private_key = :private_key WHERE id = :id');
        if ($req->execute(array("id" => $this->getId(),"ip_address" => $this->getIp(), "ssl_certificate" => $this->getCertificate(), "private_key" => $this->getPrivateKey()))) {
            return true;
        }
            throw new Exception("Failed to update domain");
    }

    public function suspendDomain(): bool {
        $req = $this->databaseInstance->prepare('UPDATE domains SET active = :active WHERE domain_name = :domain_name');
        if ($req->execute(array("active" => 0, "domain_name" => $this->getName()))) {
            return true;
        }
        throw new Exception("Failed to suspend domain");
    }

    public function removeDomain(): bool {
        if (!$this->isActive()) {
            throw new Exception("You cannot remove this domain because it is suspended. Please contact support.");
        }
        $req = $this->databaseInstance->prepare('DELETE FROM domains WHERE id = :id');
        if ($req->execute(array("id" => $this->getId()))) {
            return true;
        }
        throw new Exception("Failed to delete domain");
    }

    public function removeDomainRule(): void {
        unlink("/etc/nginx/sites-enabled/". $this->getName(). "");
        unlink("/etc/nginx/ssl/" . $this->getName() . ".crt");
        unlink("/etc/nginx/ssl/" . $this->getName() . ".key");

    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        if (Validation::verifyDomain($name)) {
            $this->name = $name;
        } else {
            throw new Exception("Domain name is not valid");
        }
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        if (Validation::verifyIP($ip)) {
            $this->ip = $ip;
        } else {
            throw new Exception("Entered IPv4 address is not valid");
        }
    }

    /**
     * @return string
     */
    public function getCertificate(): string
    {
        return $this->certificate;
    }

    /**
     * @param string $certificate
     */
    public function setCertificate(string $certificate): void
    {
        $this->certificate = $certificate;
    }

    /**
     * @return String
     */
    public function getPrivateKey(): string
    {
        return $this->private_key;
    }

    /**
     * @param String $private_key
     */
    public function setPrivateKey(string $private_key): void
    {
        $this->private_key = $private_key;
    }




    public function formatSSLConfig(): void {
        if (empty($this->getCertificate()) && empty($this->getPrivateKey())) {
            $this->setConfig("server {
	listen 80;
	listen [::]:80;

	server_name " . $this->getName() . ";

	location / {
	    proxy_set_header Host \$host;
		proxy_pass http://" . $this->getIp() . ":80;
	}
}");
        } else {
            $this->setConfig("
server {
listen 80;
server_name " . $this->getName() . ";
return 301 https://" . $this->getName(). "\$request_uri;
}            
            
server {
listen 443;
ssl on;
ssl_certificate /etc/nginx/ssl/" . $this->getName() . ".crt;
ssl_certificate_key /etc/nginx/ssl/" . $this->getName(). ".key;
server_name " . $this->getName() . ";
location / {
	    proxy_set_header Host \$host;
		proxy_pass http://" . $this->getIp() . ":80;
}
}");
        }

    }

    public function createRule(): void {
        $file = fopen("/etc/nginx/sites-enabled/". $this->getName() . "", "w") or throw new Exception("Failed to configure domain");
        fwrite($file, $this->getConfig());
        fclose($file);
        if (!empty($this->getCertificate()) && !empty($this->getPrivateKey())) {
            $file = fopen("/etc/nginx/ssl/" . $this->getName() . ".crt", "w") or throw new Exception("Failed to configure domain");
            fwrite($file, $this->getCertificate());
            fclose($file);

            $file = fopen("/etc/nginx/ssl/" . $this->getName() . ".key", "w") or throw new Exception("Failed to configure domain");
            fwrite($file, $this->getPrivateKey());
            fclose($file);
        }
    }

    /**
     * @return String
     */
    public function getConfig(): string
    {
        return $this->config;
    }

    /**
     * @param String $config
     */
    public function setConfig(string $config): void
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getAllDomains(int $userid): Array
    {
        $list = [];
        $req = $this->databaseInstance->prepare('SELECT * FROM domains WHERE user = :user');
        $req->execute(array('user' => $userid));
        foreach ($req->fetchAll() as $domain) {
            $list[] = $this->factoryConstruct($domain['id'], $domain['domain_name'], $domain['ip_address'], $domain['ssl_certificate'], $domain['private_key'], $domain['active']);
        }
        return $list;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setID (int $id) : void {
        if (intval($id)) {
            $this->id = $id;
        } else {
            throw new Exception("Something went wrong. Contact an administrator");
        }
    }

    public function factoryConstruct(int $id, string $name, string $ip, string $certificate, string $private_key, bool $active)
    {
        $domain = new Domain();
        $domain->id = $id;
        $domain->name = $name;
        $domain->ip = $ip;
        $domain->certificate = $certificate;
        $domain->private_key = $private_key;
        $domain->active = $active;
        return $domain;
    }


}