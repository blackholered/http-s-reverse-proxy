<?php
require_once('utilities/Validation.php');
class User
{

    private int $id;
    private String $username;
    private String $password;
    private bool $admin;
    private PDO $databaseInstance;


    public function __construct()
    {
        $this->databaseInstance = DatabaseConnection::getInstance();

    }

    /**
     * @return PDO|void|null
     */
    public function getDatabaseInstance(): ?PDO
    {
        return $this->databaseInstance;
    }

    /**
     * @param PDO|void|null $databaseInstance
     */
    public function setDatabaseInstance(?PDO $databaseInstance): void
    {
        $this->databaseInstance = $databaseInstance;
    }




    public function getUsername(): String
    {
        return $this->username;
    }

    public function getPassword(): String
    {
        return $this->password;
    }

    public function setID (int $id) : void {
        if (intval($id)) {
            $this->id = $id;
        } else {
            throw new Exception("Something went wrong. Contact an administrator");
        }
    }
    public function getID(): int {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getAdmin(): bool
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     */
    public function setAdmin(bool $admin): void
    {
        $this->admin = $admin;
    }




    public function updatePassword(): bool {
        $req = $this->databaseInstance->prepare('UPDATE users SET password = :password WHERE id = :id');
        if ($req->execute(array("password" => $this->getPassword(),"id" => $this->getID()))) {
            return true;
        }
        throw new Exception("Failed to update password");
    }

    public function removeUser(): bool {
        $req = $this->databaseInstance->prepare('DELETE FROM users WHERE id = :id');
        if ($req->execute(array("id" => $this->getID()))) {
            return true;
        }
        throw new Exception("Failed to delete account");
        }




    public function setUsernameLogin(String $username): void {
        $this->username = htmlspecialchars($username);
    }

    public function setPasswordLogin(String $password): void
    {
        $this->password = htmlspecialchars($password);
    }


    /**
     * @throws Exception
     */
    public function setUsernameCreate(String $username): void
    {
        $username = htmlspecialchars($username);
        if (Validation::validateUsername($username)) {
            $this->username = $username;
        } else {
            throw new Exception("Username is not valid");
        }
    }


    /**
     * @throws Exception
     */
    public function setPasswordCreate(String $password): void
    {
        $password = htmlspecialchars($password);
        if (Validation::validatePassword($password)) {
            $this->password = Validation::hashPassword($password);
        } else {
            throw new Exception("Password is not valid.");
        }
    }


}